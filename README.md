#What is a progressive web app?

Progressive Web Apps in simple words combine the best experiences of the web and of apps. They behave like native mobile apps, by taking advantage of modern browser features.


##Here is a list of 10 key features that seperate progressive web apps (PWA) from normal websites (Source: Google Developers website.)

Progressive - Works for every user, regardless of browser choice because it's built with progressive enhancement as a core tenet.

Responsive - Fits any form factor: desktop, mobile, tablet, or whatever is next.

Connectivity independent - Enhanced with service workers to work offline or on low-quality networks.

App-like - Feels like an app to the user with app-style interactions and navigation because it's built on the app shell model.

Fresh - Always up-to-date thanks to the service worker update process.

Safe - Served via HTTPS to prevent snooping and to ensure content hasn't been tampered with.

Discoverable - Is identifiable as an "application" thanks to W3C manifest and service worker registration scope, allowing search engines to find it.

Re-engageable - Makes re-engagement easy through features like push notifications.

Installable - Allows users to "keep" apps they find most useful on their home screen without the hassle of an app store.

Linkable - Easily share via URL, does not require complex installation.



##To learn more and start coding:
Listen to this talk by Andrew Mori, director of technology at konga. If you need to convince some business dev team why tomake the change, this should help: https://www.youtube.com/watch?v=f8Bhbbsl2HM

Your first progressive web app by Pete LePage, a developer advocate at google : https://developers.google.com/web/fundamentals/getting-started/codelabs/your-first-pwapp/

Build a seflie progressive web app by by Danny Markov : http://tutorialzine.com/2016/09/everything-you-should-know-about-progressive-web-apps/

Free Udacity course, Intro to progressive web app by Google: https://www.udacity.com/course/intro-to-progressive-web-apps--ud811

Great introduction to progressive web apps on smashing magazine

by Kevin Farrugia : https://www.smashingmagazine.com/2016/08/a-beginners-guide-to-progressive-web-apps/

by Ada Rose Edwards: https://www.smashingmagazine.com/2016/09/the-building-blocks-of-progressive-web-apps/

Progressive Web Apps with React.js: https://medium.com/@addyosmani/progressive-web-apps-with-react-js-part-i-introduction-50679aef2b12#.4j3147mfg

Building Progressive Web Apps (Webinar and Tutorial) : https://vaadin.com/blog/-/blogs/building-progressive-web-apps-webinar-and-tutorial



##Open source pwa projects to play with while learning
https://github.com/gokulkrishh/demo-progressive-web-app

Using laravel and angular material: https://www.laravel-angular.io/#/ 

Using vuejs: https://github.com/BosNaufal/vue-simple-pwa



##Need further convincing? 
Check out this selction of progressive web apps: https://pwa.rocks/ and https://kongax.konga.com/
